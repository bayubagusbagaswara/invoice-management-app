# Invoice Management Application

## Langkah 1

- Membuat skema database di `db.migration`
- Karena seharusnya kita sudah mendesign databasenya
- Lalu biasanya kita set `ddl-auto` menjadi `validate`
- Tujuannya agar mengamankan data-data didatabase, agar tidak bisa diubah dari kode program kita
- Kemudian, baru kita buat class entity yang berguna untuk entitas representasi data dari database
- Alhasil semua property dari class entity harus sama dengan kolom yang sudah didesign di database
- Sehingga, setiap ada ketidaksamaan property antara class entity dengan kolom database, maka akan langsung error

## Skema Database Awal

```text
DATABASE_NAME = invoicedb
DATABASE_USERNAME = invoice
DATABASE_PASSWORD = invoice
```

## Setting application.properties
```text
spring.datasource.url=jdbc:postgresql://localhost:5432/invoicedb
spring.datasource.username=invoice
spring.datasource.password=invoice

spring.jpa.hibernate.ddl-auto=validate
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.format_sql=true
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQL10Dialect
```