
-- INVOICE TYPE
create table invoice_type (
    id character varying(36) NOT NULL,
    created timestamp without time zone,
    created_by character varying(255),
    status_record character varying(255) NOT NULL,
    updated timestamp without time zone,
    updated_by character varying(255),
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL
);

alter table only invoice_type
    add constraint invoice_type_pkey primary key (id);

-- INVOICE
create table invoice (
    id character varying(36) NOT NULL,
    created timestamp without time zone,
    created_by character varying(255),
    status_record character varying(255) NOT NULL,
    updated timestamp without time zone,
    updated_by character varying(255),
    amount numeric(19,2) NOT NULL,
    description character varying(255) NOT NULL,
    due_date date NOT NULL,
    invoice_number character varying(100) NOT NULL,
    paid boolean NOT NULL,
    payment_status character varying(255) NOT NULL,
    total_payment numeric(19,2) NOT NULL,
    id_invoice_type character varying(255) NOT NULL,
    CONSTRAINT invoice_amount_check CHECK ((amount >= (0)::numeric)),
    CONSTRAINT invoice_total_payment_check CHECK ((total_payment >= (0)::numeric))
);

alter table only invoice
    add constraint invoice_pkey primary key (id);

alter table only invoice
    add constraint invoice_unique_number unique (invoice_number);

alter table only invoice
    add constraint fk_invoice_invoice_type foreign key (id_invoice_type) references invoice_type (id);

-- PAYMENT PROVIDER
create table payment_provider (
    id character varying(36) NOT NULL,
    created timestamp without time zone,
    created_by character varying(255),
    status_record character varying(255) NOT NULL,
    updated timestamp without time zone,
    updated_by character varying(255),
    code character varying(100) NOT NULL,
    logo character varying(255),
    name character varying(100) NOT NULL
);

alter table only payment_provider
    add constraint payment_provider_pkey primary key (id);

alter table only payment_provider
    add constraint payment_provider_unique_code unique (code);

-- VIRTUAL ACCOUNT
create table virtual_account (
    id character varying(36) NOT NULL,
    created timestamp without time zone,
    created_by character varying(255),
    status_record character varying(255) NOT NULL,
    updated timestamp without time zone,
    updated_by character varying(255),
    account_number character varying(255) NOT NULL,
    company_id character varying(255) NOT NULL,
    payment_type character varying(255) NOT NULL,
    id_invoice character varying(255) NOT NULL,
    id_payment_provider character varying(255) NOT NULL
);

alter table only virtual_account
    add constraint virtual_account_pkey primary key (id);

alter table only virtual_account
    add constraint virtual_account_unique_account_number unique (account_number);

alter table only virtual_account
    add constraint fk_virtual_account_invoice foreign key (id_invoice) references invoice(id);

alter table only virtual_account
    add constraint fk_virtual_account_payment_provider foreign key (id_payment_provider) references payment_provider(id);

-- PAYMENT
create table payment (
    id character varying(36) NOT NULL,
    created timestamp without time zone,
    created_by character varying(255),
    status_record character varying(255) NOT NULL,
    updated timestamp without time zone,
    updated_by character varying(255),
    amount numeric(19,2) NOT NULL,
    provider_reference character varying(255) NOT NULL,
    transaction_fee numeric(19,2) NOT NULL,
    transaction_time timestamp without time zone NOT NULL,
    id_virtual_account character varying(255) NOT NULL,
    CONSTRAINT payment_amount_check CHECK ((amount >= (1)::numeric)),
    CONSTRAINT payment_transaction_fee_check CHECK ((transaction_fee >= (0)::numeric))
);

alter table only payment
    add constraint payment_pkey primary key (id);

alter table only payment
    add constraint fk_payment_virtual_account foreign key (id_virtual_account) references virtual_account (id);

create table invoice_type_provider (
    id_invoice_type character varying(255) NOT NULL,
    id_payment_provider character varying(255) NOT NULL
);

alter table only invoice_type_provider
    add constraint invoice_type_provider_pkey primary key (id_invoice_type, id_payment_provider);

alter table only invoice_type_provider
    add constraint fk_invoice_type_provider_provider foreign key (id_payment_provider) references payment_provider(id);

alter table only invoice_type_provider
    add constraint fk_invoice_type_provider_type foreign key (id_invoice_type) references invoice_type(id);