package com.invoice.management.entity.dataEnum;

public enum PaymentType {
    CLOSED, OPEN, INSTALLMENT
}
