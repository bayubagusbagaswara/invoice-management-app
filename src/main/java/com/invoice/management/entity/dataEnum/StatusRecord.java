package com.invoice.management.entity.dataEnum;

public enum StatusRecord {
    ACTIVE, INACTIVE
}
