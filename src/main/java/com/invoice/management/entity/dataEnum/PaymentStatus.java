package com.invoice.management.entity.dataEnum;

public enum PaymentStatus {
    NONE, PARTIAL, FULL
}
